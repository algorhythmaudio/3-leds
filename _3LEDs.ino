int switchState = 0;

void setup() {
  pinMode(2, INPUT);
  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

void loop() {
  switchState = digitalRead(2);

  if (switchState == LOW) { // Button Not Pressed
    digitalWrite(3, HIGH); // Green
    digitalWrite(4, LOW); // Red 1
    digitalWrite(5, LOW); // Red 2
  }
  
  else { // Button pressed
    digitalWrite(3, LOW);
    digitalWrite(4, LOW);
    digitalWrite(5, HIGH);

    delay(250);
    digitalWrite(4, HIGH);
    digitalWrite(5, LOW);
    delay(250);
  }
}
